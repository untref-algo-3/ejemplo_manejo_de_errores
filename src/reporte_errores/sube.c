#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "sube.h"

bool fechaValida(const char *fecha) {
  return false;
  // return true;
}

bool dniValido(const char *dni) {
  return false;
  // return true;
}

int buscarUsuario(const char *dni, Usuario **usuarioEncontrado) {
  if (!dniValido(dni)) {
    return E_DNI_INVALIDO;
  }

  // Aca se buscaria el usuario pero a modo de ejemplo creamos un usuario
  *usuarioEncontrado = malloc(sizeof(Usuario));
  if (*usuarioEncontrado == NULL) {
    return E_ERROR_DE_SISTEMA;
  }

  (*usuarioEncontrado)->cantidadViajes = 16;
  (*usuarioEncontrado)->dni = malloc(sizeof(char) * (strlen(dni) + 1));
  strcpy((*usuarioEncontrado)->dni, dni);

  return 0;
}

int cantidadDeViajes(const char *fecha, const char *dni) {
  if (!fechaValida(fecha)) {
    return E_FECHA_INVALIDA;
  }

  Usuario *usuario;
  int resultadoBusquedaUsuario = buscarUsuario(dni, &usuario);
  if (resultadoBusquedaUsuario != 0) {
    return resultadoBusquedaUsuario;
  }

  int cantViajes = usuario->cantidadViajes;
  free(usuario);

  return cantViajes;
}
