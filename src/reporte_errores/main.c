#include <stdio.h>
#include "sube.h"

int main() {
  int viajes = cantidadDeViajes("2020-01-01", "22333444");

  // Chequeo de error
  if (viajes < 0) {
    switch (viajes)
    {
    case E_FECHA_INVALIDA:
      printf("La fecha provista es invalida\n");
      break;
    case E_DNI_INVALIDO:
      printf("El DNI provisto es invalido\n");
      break;
    case E_USUARIO_NO_ENCONTRADO:
      printf("No se encontro el usuario\n");
      break;

    default:
      printf("Error desconocido\n");
    }

    return viajes;
  }

  printf("La cantidad de viajes para el usuario con DNI 22333444 fue %d\n", viajes);

  return 0;
}
