#ifndef _SUBE_H_

#define _SUBE_H_

#define E_FECHA_INVALIDA -1000
#define E_DNI_INVALIDO -1001
#define E_USUARIO_NO_ENCONTRADO -1002
#define E_ERROR_DE_SISTEMA -1100

typedef struct {
  char *dni;
  int cantidadViajes;
} Usuario;

int buscarUsuario(const char *dni, Usuario **usuarioEncontrado);
int cantidadDeViajes(const char *fecha, const char *dni);

#endif
