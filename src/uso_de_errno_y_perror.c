#include <errno.h>

#include <stdlib.h>
#include <stdio.h>

int main() {
  errno = 0;

  char *string_gigante = malloc(sizeof(char) * 64000000000); // Intentamos reservar 64 GB de memoria

  if (string_gigante == NULL) {
    printf("El valor de errno es: %d\n", errno);        // Salida a STDOUT
    perror("Falló la alocación de memoria con malloc"); // Salida a STDERR
  }

  return 1;
}
