#include <stdlib.h>
#include <stdio.h>

int main() {
  char *string_gigante = malloc(sizeof(char) * 64000000000); // Intentamos reservar 64 GB de memoria

  if (string_gigante == NULL) {
    printf("Falló la reservación de memoria con malloc\n");
  }

  int resultado_atoi = atoi("no-es-un-entero");
  if (!resultado_atoi) {
    printf("No pudo convertir el string a entero\n");
  }

  return 1;
}
