# Ejemplos para la clase de manejo de errores en C

## Chequeo de valores de retorno de función

El código de `chequeo_de_valores_de_retorno.c` demuestra como se efectúa el chequeo de valores devueltos por funciones de la librería estándar.

## Uso de `errno` y `perror`

Usando como ejemplo el código de `uso_de_errno_y_perror.c` podemos ver como funciona `errno` y `perror` en la práctica.

Para eso primero compilar el código con: `gcc src/uso_de_errno_y_perror.c -o out/uso_de_errno_y_perror`

#### Invocado sin redireccionar streams

Al no redireccionar los streams, `STDOUT` y `STDERR` imprimen en pantalla:
```
~ out/uso_de_errno_y_perror
El valor de errno es: 12
Falló la alocación de memoria con malloc: Cannot allocate memory
```

#### Redireccionando STDERR a `/dev/null`

Al redireccionar `STDERR` a `/dev/null`, `STDOUT` imprime en pantalla pero `STDERR` no se imprime ni se almacena en ningún archivo:
```
~ out/uso_de_errno_y_perror 2>/dev/null
El valor de errno es: 12
```

#### Redireccionando STDERR a un archivo de logs

Al redireccionar `STDERR` a un archivo, `STDOUT` imprime en pantalla y `STDERR` se almacena en el archivo `/tmp/uso_de_errno_y_perror.log`:
```
~ out/uso_de_errno_y_perror 2>/tmp/uso_de_errno_y_perror.log
El valor de errno es: 12
~ cat /tmp/uso_de_errno_y_perror.log
Falló la alocación de memoria con malloc: Cannot allocate memory
```

#### Redireccionando STDOUT a `/dev/null`

Al redireccionar `STDOUT` a `/dev/null`, se imprime en pantalla `STDERR` pero `STDOUT` no se imprime ni se almacena en ningún archivo:
```
~ out/uso_de_errno_y_perror 1>/dev/null
Falló la alocación de memoria con malloc: Cannot allocate memory
```

## Reporte de errores en código propio

El código del directorio `reporte_errores` es un ejemplo de como hacer el reporte y manejo de errores en el código propio. Para compilarlo:

`gcc src/reporte_errores/sube.c src/reporte_errores/main.c -o out/sube`
